package com.udeo.futbol.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity(name="partidos")
@Table(name="partidos")
public class Partido {
/*PK == Primary Key*/
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="numanotaciones")
	private int numanotaciones;
	
	
	@Column(name="numtarjetas")
	private int numtarjetas;
	
	
	@Column(name="numjugadores")
	private int numjugadores;
	
	/*parametros nombrados*/
	public Partido() {}
	public Partido(int id, String nombre, int numanotaciones, int numtarjetas, int numjugadores) {
		this.id = id;
		this.nombre = nombre;
		this.numanotaciones = numanotaciones;
		this.numtarjetas = numtarjetas;
		this.numjugadores = numjugadores;
	}
	/*GET*/
	
	public int getId() {
		return this.id;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public int getNumanotaciones() {
		return this.numanotaciones;
	}

	public int getNumtarjetas() {
		return this.numtarjetas;
	}
	
	public int getNumJugadores() {
		return this.numjugadores;
	}
	/*SET*/
	
	public void setId(int id) {
		this.id = id;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setNumnotaciones(int numanotaciones) {
		this.numanotaciones = numanotaciones;
	}
	public void setNumtarjetas(int numtarjetas) {
		this.numtarjetas = numtarjetas;
	}
	public void setNumjugadores(int numjugadores) {
		this.numjugadores = numjugadores;
	}
	
}
