package com.udeo.futbol.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.udeo.futbol.Entity.Partido;
import com.udeo.futbol.service.PartidoService;
//Indiciamos que es un controlador rest
@RestController
@RequestMapping("/futbol") //esta sera la raiz de la url, es decir http://127.0.0.1:8080/api/

public class PartidoRestController {
	
	//Inyectamos el servicio para poder hacer uso de el
	@Autowired
	private PartidoService partidoService;

	/*Este método se hará cuando por una petición GET (como indica la anotación) se llame a la url 
	http://127.0.0.1:8080/api/users*/
	@GetMapping("/partidos")
	public List<Partido> findAll(){
		//retornará todos los usuarios
		return partidoService.findAll();
	}
	
	@GetMapping("/partidos/{partidoId}")
	public Partido getPartido(@PathVariable int partidoId) {
	Partido partido = partidoService.findById(partidoId);
	if (partido == null) {
	throw new ResponseStatusException(
	HttpStatus.NOT_FOUND, "No Existe el partido");
	}
	return partido;
	}

	@PostMapping("/partidos")
	public Partido addPartido(@RequestBody Partido partido) {
	partido.setId(0);
	partidoService.save(partido);
	return partido;
	}
	
	@PutMapping("/partidos/{partidoId}")
	public Partido updatePartido(@RequestBody Partido partido, @PathVariable int partidoId) {
	partido.setId(partidoId);
	partidoService.save(partido);
	return partido;
	}
	
	@DeleteMapping("/partidos/{partidoId}")
	  void deletePartido(@PathVariable int partidoId) {
	    partidoService.deleteById(partidoId);
	  }
	
	
	
}