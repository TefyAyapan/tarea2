package  com.udeo.futbol.service;

import java.util.List;

import com.udeo.futbol.Entity.Partido;

public interface PartidoService {
	
	public List<Partido> findAll();
	
	public Partido findById(int id);
	
	public void save (Partido partido);
	
	public void deleteById (int id);

}
