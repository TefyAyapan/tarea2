package com.udeo.futbol.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udeo.futbol.Entity.Partido;
import com.udeo.futbol.dao.PartidoDao;

 

@Service
public class PartidoServiceImpl implements PartidoService {

	@Autowired
	private PartidoDao userDAO;
	
	@Override
	public List<Partido> findAll() {
		List<Partido> listUsers= userDAO.findAll();
		return listUsers;
	}

	@Override
	public Partido findById(int id) {
		Partido partido = userDAO.findById(id);
		return partido;
	}

	@Override
	public void save(Partido partido) {
		userDAO.save(partido);

	}

	@Override
	public void deleteById(int id) {
		userDAO.delete(id);
	}

}