package com.udeo.futbol.dao;
import java.util.List;

import com.udeo.futbol.Entity.Partido;

public interface PartidoDao {
	public List<Partido> findAll();
	
	public Partido findById(int id);
	
	public void save (Partido partido);
	
	public void delete (int id);
}
